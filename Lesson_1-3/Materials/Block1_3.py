residence_limit = 90  # 45, 60
schengen_constraint = 180

# Мы тратим в день
cost_per_day = 15

first_of_january = 1  # первое января текущего года

first_time_arriving = 1
first_time_leave = 11

second_time_arriving = 16
second_time_leave = 27

third_time_arriving = 45
third_time_leave = 46

total_time_in_es = (first_time_leave - first_time_arriving)
total_time_in_es = total_time_in_es + (second_time_leave - second_time_arriving)
total_time_in_es = total_time_in_es + (third_time_leave - third_time_arriving)

if total_time_in_es > residence_limit:
    print('Вы не можете прибывать в ЕС так долго')

print('Вы пробудете в ЕС дней:', total_time_in_es)
print('Стоимость проживания для вас:', total_time_in_es * cost_per_day, 'евро')

olya_budget = sum([300, 500, 12, 50, 12])

oleg_salary = [12, 300, 50, 45]
oleg_average_salary = sum(oleg_salary) / len(oleg_salary)
oleg_budget = oleg_average_salary * 5

budget = olya_budget + oleg_budget
personal_expenditures = 500
common_budget = (olya_budget + oleg_budget) - personal_expenditures * 2
common_budget_for_person_per_day = common_budget / total_time_in_es / 2
print('Персональный бюджет на день:', common_budget_for_person_per_day, 'евро')
if cost_per_day > common_budget_for_person_per_day:
    print('Похоже, нам не хватает денег на жизнь')

# ---------------------------------------
# А разработка настоящего шенгенского калькулятора
# ---------------------------------------

# ------- 1 --------
# Используя полученные знания,
# Изменим наш способ хранения посещений Шенгена,
# Чтобы иметь возможность проще добавлять новые посещения проще,
# Организуем из них список, рассказываем про операцию []
print('#1')
residence_limit = 90  # 45, 60
schengen_constraint = 180
visits = [[1, 10], [30, 54], [60, 89], [120, 159]]
total_time_in_es = 0
for visit in visits:
    total_time_in_es += visit[1] - visit[0] + 1

overstay_time = total_time_in_es - residence_limit
print('Вы привысили время пребывания', overstay_time)
# рассказать, что assert в данном случае будет являться нашей спецификацией
assert (overstay_time == 10 + 25 + 30 + 40 - residence_limit)

# ------- 2 --------
# Теперь будем считать ограничение 180 дней по-честному
# дело в том, что 180 дней считаются не совсем с первого въезда
# Допустим я въезжаю 18 августа, мне нужно посчитать,
# сколько времени я действительно пребывал в шенгене за последние 180 дней
# как мне это сделать? Давайте считать не общее время пребывания,
# а время пребывания на определённую дату
# Рассказать про накопление суммы, возможно привести пример в excel
# рассказать про метод append
# обратить внимание на вложенные циклы
print('#2')
residence_limit = 90  # 45, 60
schengen_constraint = 180
visits = [[1, 10], [61, 90], [101, 140], [141, 160]]
days_for_visits = []
total_time_in_es = 0
for visit in visits:
    days_for_visit = 0
    for past_visit in visits:
        if past_visit[0] < visit[0]:
            # считаем тольво визиты вполть до текущего
            days_for_visit += past_visit[1] - past_visit[0] + 1

    days_for_visit += visit[1] - visit[0] + 1
    days_for_visits.append(days_for_visit)

print (days_for_visits)
assert (days_for_visits == [10, 10 + 30, 10 + 30 + 40, 10 + 30 + 40 + 20])

# Теперь мы можем глазами увидеть превышение времени пребывания в шенгене
# Но лучше, если программа сама будет нас оповещать
# Выведем ошибку, если мы пребываем дольше 90 дней в один из приездов
# рассказываем про функцию zip
# в этот момент поговорить про области видимости переменных на примере overstay_time
for visit, total_days in zip(visits, days_for_visits):
    if total_days > residence_limit:
        overstay_time = total_days - residence_limit
        print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')

# ------- 3 --------
# Но мы помним, что 180 дней отсчитывается с планируемого заезда,
# то есть, если мы приедем 271-290,
# мы не попадаем под ограничение
# давайте поддержим такой сценарий,
    # меняем assert
    # добавляем [271, 290]
    # обавляем условие в цикл

print('#3')
residence_limit = 90  # 45, 60
schengen_constraint = 180
visits = [[1, 10], [61, 90], [101, 140], [141, 160], [271, 290]]
days_for_visits = []
total_time_in_es = 0
for visit in visits:
    days_for_visit = 0
    for past_visit in visits:
        if past_visit[0] < visit[0] and (visit[0] - schengen_constraint < past_visit[0]):
            # показываем, что можно переписать это условие без and как:
            # if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
            # готоворим о возможных проблемах ±1 в условиях
            days_for_visit += past_visit[1] - past_visit[0] + 1
    days_for_visit += visit[1] - visit[0] + 1
    days_for_visits.append(days_for_visit)

print (days_for_visits)
assert (days_for_visits == [10, 10 + 30, 10 + 30 + 40, 10 + 30 + 40 + 20, 40 + 20 + 20])

for visit, total_days in zip(visits, days_for_visits):
    if __name__ == '__main__':
        if total_days > residence_limit:
            overstay_time = total_days - residence_limit
            print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')

            # ДЗ:
            # 0.
            # В качестве подсказки можно использовать описание работы шенгенского калькулятора
            # http://www.travel.ru/formalities/visa/schengen/schengen_calculator.html
            # 1.
            # Что будет если в нашем списке будут накладывающиеся визиты?
            # Такой ситуации допустить нельзя!
            # Вывести ошибку, если есть пересекающиеся визиты.
            # Прекратить выполнение программы, команда exit()
            # 2.
            # Я планирую ещё один визит в будущем.
            # Я знаю дату въезда.
            # Сколько времени я смогу пробыть в шенгене?
            # 3.
            # разобраться с циклом while, придумать пример использования

