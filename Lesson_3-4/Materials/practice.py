import osa
import suds

url = 'http://www.webservicex.net/ConvertTemperature.asmx?WSDL'

##### osa
client1 = osa.client.Client(url)
print(client1.types)

response1 = client1.service.ConvertTemp(Temperature=15.00, FromUnit='degreeFahrenheit' , ToUnit='degreeCelsius')

print(response1)

##### suds
client2 = suds.Client(url)

response2 = client2.service.ConvertTemp(Temperature=15.00, FromUnit='degreeFahrenheit' , ToUnit='degreeCelsius')

print(response2)


# bank http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL

url2 = 'http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL'

client3 = suds.Client(url2)
response3 = client3.service.Currencies()
print(response3.split(';'))

response4 = client3.service.ConvertToNum(fromCurrency='EUR', toCurrency='RUB', amount=126.50, rounding=False)
print(response4)



def count_exp(sums):
    url = 'http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL'
    client = suds.Client(url)
    resp = 0
    for amount, currency in sums:
        # resp = resp + x same resp += x
        print('!!!', amount, currency)
        resp += client.service.ConvertToNum(fromCurrency=currency, toCurrency='RUB', amount=amount, rounding=True)
    return resp

test = [(120.00, 'USD'), (23.06, 'EUR')]
print(count_exp(test))

###
url4 = 'http://www.webservicex.net/ConvertComputer.asmx?WSDL'

client4 = osa.client.Client(url4)

response4 = client4.service.ChangeComputerUnit(ComputerValue=1024.00, fromComputerUnit='Terabyte', toComputerUnit='Byte')

print(response4)