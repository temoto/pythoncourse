## 4.1. ����������� ��� ����������� ������ � �������
### ���� �������
- ���������� �� ������ Anaconda
- ����� ����������� ���� (jupiter, qtconsole, spyder)
- �������� � ��������� ������
- ��� ����� Series, DataFrame
- ���������� �������� � �������: ����������, ����������, ����������������
- ����������� �������� ������ � ����������� �� ������ �����
- ���������� ����������
- ������� �� Excel, �� ���� ������

### �������� � ������
 - ������ [� ����� ����](https://catalog.data.gov/dataset/baby-names-from-social-security-card-applications-national-level-data), ���������, ������� � ������� ������������
 - ����� ������ �� ��� ������ ����, ����������
 - ��������� ��������� ���� � �������� ��������� ������������
 - ��������� ��������� � ������ �������

### ������ ����� �� �����������
 
 - ������������ jupiter, ������������ �������� ���������
 - ��������� ������ �� CSV
 - �������� ������ �� �����/�� ���������� �����
 - �������� ����������� ��������
 - �������� ����������� � ����������� ������������


### �������� ������

- [10 minutes to Pandas](http://pandas.pydata.org/pandas-docs/stable/10min.html)
- [Python Data Science Handbook](https://www.safaribooksonline.com/library/view/python-data-science/9781491912126/)
- [Python for Data Analysis](http://www.cin.ufpe.br/~embat/Python%20for%20Data%20Analysis.pdf)
- [�������� ��������](https://docs.google.com/spreadsheets/d/1ZSLP1McnXv0FtOd9t7dMp3AfaiusvaGwWV0F9g2pbho/edit#gid=0)
